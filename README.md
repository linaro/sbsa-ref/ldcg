# CI definition for building EDK2 firmware for SBSA Reference Platform (sbsa-ref in QEMU)

In this repository we are holding "build-firmware.sh" script used to build firmware for
SBSA Reference Platform machine (sbsa-ref in QEMU).

## Versions used

Versions of Trusted Firmware (TF-A) and Tianocore EDK2 stable release are defined as
CI/CD variables in repository settings.

For example current (March 2024) builds are done using:

- Trusted Firmware 2.10.2
- Tianocore EDK2 stable202402

Code from edk2-platforms is used "as-is" as there are no tags in that repository.

## Artifacts

Resulting files are uploaded to
[CodeLinaro Artifacts](https://artifacts.codelinaro.org/ui/native/linaro-419-sbsa-ref/)
into DATE-JOB_ID directories. The "latest/" directory points into newest build.

Each build generates README.txt file listing:

- TF-A and EDK2 releases
- commit from edk2-platforms
- toolchain Versions
- checksums for each artifact
