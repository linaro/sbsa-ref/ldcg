#!/bin/bash

install_deps()
{
    # in case someone runs it by hand in debian:bookworm container
    if [ `whoami` == 'root' ]; then
        apt update
        apt install -y sudo
    fi

    sudo apt update
    sudo apt -y --no-install-recommends install \
            build-essential gcc-aarch64-linux-gnu \
            ca-certificates curl git wget \
            acpica-tools libssl-dev uuid-dev \
            libglib2.0-dev ninja-build \
            python3 python3-pytest python3-requests \
            python3-venv python3-yaml \
            xz-utils
}

get_firmware()
{
    mkdir -p sbsa-ref-status/firmware
    rm sbsa-ref-status/firmware/SBSA*
    wget -O sbsa-ref-status/firmware/SBSA_FLASH0.fd.xz https://artifacts.codelinaro.org/artifactory/linaro-419-sbsa-ref/latest/edk2/SBSA_FLASH0.fd.xz
    wget -O sbsa-ref-status/firmware/SBSA_FLASH1.fd.xz https://artifacts.codelinaro.org/artifactory/linaro-419-sbsa-ref/latest/edk2/SBSA_FLASH1.fd.xz
    xz -d sbsa-ref-status/firmware/SBSA*xz
}

fetch_qemu()
{
    git clone --depth 1 https://gitlab.com/qemu-project/qemu.git
}


build_qemu()
{
    cd $ROOT/qemu && \
    ./configure \
        --target-list=aarch64-softmmu,aarch64-linux-user \
        --with-devices-aarch64=minimal \
        --disable-gtk \
        --disable-vte \
        --disable-plugins \
        --disable-alsa \
        --disable-bzip2 \
        --disable-pa \
        --disable-oss  \
        --disable-parallels \
        --disable-dmg \
        --disable-curl \
        --disable-lzo \
        --disable-zstd \
        --disable-werror \
        --enable-slirp \
        --disable-docs && \
    ninja -C build
    ln -sf $PWD/build/qemu-system-aarch64 $ROOT/sbsa-ref-status/bin/qemu-system-aarch64
}


fetch_sbsa_ref_status()
{
    git clone https://github.com/hrw/sbsa-ref-status.git
    mkdir -p $ROOT/sbsa-ref-status/bin
}


test_sbsa_ref()
{
    cd $ROOT/sbsa-ref-status

    for os in alpine centos9 debian10 debian12 debian13 \
              freebsd13 freebsd14 freebsd15 \
              netbsd9 netbsd10 netbsd11 \
              openbsd76
    do
        echo ""
        ./fetch-os-images.py --quiet $os
        pytest -v --no-header --no-summary -k $os
        rm -f disks/*{img,iso,qcow}*
        echo ""
    done
}

ROOT=$PWD

install_deps
fetch_sbsa_ref_status
get_firmware
fetch_qemu
build_qemu
test_sbsa_ref
