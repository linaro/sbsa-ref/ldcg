#!/bin/bash

#TFA_VERSION="v2.11.0"
#EDK2_VERSION="4d4f56992460c039d0cfe48c394c2e07aecf1d22"
#
# On CodeLinaro CI those variables are set in Settings -> CI/CD:
# https://git.codelinaro.org/linaro/sbsa-ref/ldcg/-/settings/ci_cd

set -ex

build_tfa()
{
    cd ${WORKSPACE}/trusted-firmware
    CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm make -j$(nproc) \
    PLAT=qemu_sbsa DEBUG=0 RELEASE=1 all fip

    # emulate edk2-non-osi
    mkdir -p ${WORKSPACE}/Platform/Qemu/Sbsa
    cp build/qemu_sbsa/release/*.bin ${WORKSPACE}/Platform/Qemu/Sbsa
}

build_edk2()
{
    cd ${WORKSPACE}
    export PYTHON_COMMAND=/usr/bin/python3
    export PACKAGES_PATH=$WORKSPACE/edk2:$WORKSPACE/edk2-platforms
    make -C edk2/BaseTools -j$(nproc)

    export GCC_AARCH64_PREFIX=aarch64-linux-gnu-

    source edk2/edksetup.sh
    build -b RELEASE -a AARCH64 -t GCC -p edk2-platforms/Platform/Qemu/SbsaQemu/SbsaQemu.dsc -n 0 -s
    build -b RELEASE -a AARCH64 -t GCC -p edk2/ShellPkg/ShellPkg.dsc -m edk2/ShellPkg/Application/Shell/Shell.inf -n 0 -s
}

fetch_code()
{
    cd ${WORKSPACE}
    git clone https://review.trustedfirmware.org/TF-A/trusted-firmware-a.git trusted-firmware
    cd trusted-firmware
    git checkout ${TFA_VERSION}
    TFA_SHORT_VERSION=$(git rev-parse --short HEAD)
    cd ..

    git clone --tags https://github.com/tianocore/edk2.git
    cd edk2
    git checkout ${EDK2_VERSION}
    git submodule update --init
    EDK2_SHORT_VERSION=$(git rev-parse --short HEAD)
    cd ..

    git clone --depth 1 https://github.com/tianocore/edk2-platforms.git
    cd edk2-platforms
    EDK2_PLATFORMS_VERSION=$(git rev-parse --short HEAD)
}

install_deps()
{
    # in case someone runs it by hand in debian:bookworm container
    if [ `whoami` == 'root' ]; then
        apt update
        apt install -y sudo
    fi

    sudo apt update
    sudo apt -y --no-install-recommends install \
            build-essential gcc-aarch64-linux-gnu \
            ca-certificates curl git wget \
            acpica-tools libssl-dev uuid-dev \
            python3 \
            xz-utils
}

test_in_qemu()
{
    sudo apt -y --no-install-recommends install qemu-system-aarch64 ipxe-qemu seabios

    cd ${WORKSPACE}
    mkdir -p virtual-fat/efi/boot/

    cp Build/Shell/RELEASE_GCC/AARCH64/Shell_EA4BB293-2D7F-4456-A681-1F22F42CD0BC.efi virtual-fat/efi/boot/bootaa64.efi
    echo "echo 'hello from uefi'" >virtual-fat/startup.nsh

    cp Build/SbsaQemu/RELEASE_GCC/FV/SBSA_FLASH[01].fd .
    truncate -s 256M SBSA_FLASH[01].fd

    # if we have to terminate qemu then firmware is bad and build should fail
    timeout --foreground 60s qemu-system-aarch64 -M sbsa-ref -m 2048 \
        -drive file=SBSA_FLASH0.fd,format=raw,if=pflash \
        -drive file=SBSA_FLASH1.fd,format=raw,if=pflash \
        -drive file=fat:rw:virtual-fat/,format=raw \
        -nographic \
        -serial mon:stdio
}

prepare_publish()
{
    cd ${WORKSPACE}
    mkdir -p publish/{tf-a,edk2,efi-shell}/

    cp trusted-firmware/build/qemu_sbsa/release/*.bin publish/tf-a/

    cp Build/SbsaQemu/RELEASE_GCC/FV/SBSA_FLASH[01].fd publish/edk2
    truncate -s 256M publish/edk2/SBSA_FLASH[01].fd

    xz -T0 publish/edk2/SBSA_FLASH[01].fd

    cp Build/Shell/RELEASE_GCC/AARCH64/Shell_EA4BB293-2D7F-4456-A681-1F22F42CD0BC.efi publish/efi-shell/Shell.efi

    echo "Firmware files for SBSA Reference Platform (sbsa-ref) for QEMU CI usage." >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "Toolchain from Debian:                                                  " >> publish/README.txt
    echo "$(aarch64-linux-gnu-gcc --version | head -n1)                           " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "Used components:                                                        " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "- Trusted Firmware         ${TFA_SHORT_VERSION}                         " >> publish/README.txt
    echo "- Tianocore EDK2           ${EDK2_SHORT_VERSION}                        " >> publish/README.txt
    echo "- Tianocore EDK2-platforms ${EDK2_PLATFORMS_VERSION}                    " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "CI variables:                                                           " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "- Trusted Firmware         ${TFA_VERSION}                               " >> publish/README.txt
    echo "- Tianocore EDK2           ${EDK2_VERSION}                              " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "SHA256 checksums:                                                       " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "edk2/                                                                   " >> publish/README.txt
    echo "- SBSA_FLASH0.fd.xz: $(sha256sum < publish/edk2/SBSA_FLASH0.fd.xz)      " >> publish/README.txt
    echo "- SBSA_FLASH1.fd.xz: $(sha256sum < publish/edk2/SBSA_FLASH1.fd.xz)      " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "efi-shell/                                                              " >> publish/README.txt
    echo "- Shell.efi: $(sha256sum < publish/efi-shell/Shell.efi)                 " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
    echo "tf-a/                                                                   " >> publish/README.txt
    echo "- bl1.bin:  $(sha256sum < publish/tf-a/bl1.bin)                         " >> publish/README.txt
    echo "- bl2.bin:  $(sha256sum < publish/tf-a/bl2.bin)                         " >> publish/README.txt
    echo "- bl31.bin: $(sha256sum < publish/tf-a/bl31.bin)                        " >> publish/README.txt
    echo "- fip.bin:  $(sha256sum < publish/tf-a/fip.bin)                         " >> publish/README.txt
    echo "                                                                        " >> publish/README.txt
}

mkdir -p $WORKSPACE
cd $WORKSPACE

rm -rf *

install_deps
fetch_code

build_tfa
build_edk2

test_in_qemu
prepare_publish
